//
//  AppDelegate.h
//  ImagePicker
//
//  Created by  jueying on 15/12/23.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

