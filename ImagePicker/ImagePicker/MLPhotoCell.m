//
//  MLPhotoCell.m
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import "MLPhotoCell.h"

@interface MLPhotoCell ()
{
	void (^_block)(MLPhotoCell *);
}

@end

@implementation MLPhotoCell

- (IBAction)deleteBtnDown:(id)sender {
	if (_block) {
//		[UIView animateWithDuration:1.f delay:.1f usingSpringWithDamping:.3 initialSpringVelocity:.1 options:UIViewAnimationOptionLayoutSubviews animations:^{
//
//		} completion:^(BOOL finished) {
//
//		}];
		_block(self);
	}
}

- (void)whenDeleteBtnDown:(void (^)(MLPhotoCell *cell))block
{
	
	_block = block;
}

- (void)prepareForReuse
{
	[super prepareForReuse];
	_block = nil;
}

- (void)dealloc
{
	_block = nil;
}

@end
