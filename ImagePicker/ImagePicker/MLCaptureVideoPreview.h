//
//  MLCaptureVideoPreview.h
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MLCaptureVideoPreview : UIView

@property (nonatomic) AVCaptureSession *session;

@end
