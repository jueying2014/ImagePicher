//
//  MLPhotoCell.h
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLPhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void)whenDeleteBtnDown:(void (^)(MLPhotoCell *cell))block;

@end
