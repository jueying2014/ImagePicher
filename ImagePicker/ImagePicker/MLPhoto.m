//
//  MLPhoto.m
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import "MLPhoto.h"

@implementation MLPhoto

+ (instancetype)photoWithData:(NSData *)imageData
{
	return [[self alloc]initWithData:imageData];
}

- (instancetype)initWithData:(NSData *)imageData
{
	self = [super init];
	
	if (self) {
		_imageData = imageData;
	}
	
	return self;
}

- (UIImage *)image
{
	return [UIImage imageWithData:_imageData];
}

@end
