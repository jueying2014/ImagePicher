//
//  MLPhoto.h
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLPhoto : NSObject
{
	NSData *_imageData;
}

+ (instancetype)photoWithData:(NSData *)imageData;

@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, copy) NSString *location;

- (void)save;

@end
