//
//  MLCaptureVideoPreview.m
//  ImagePicker
//
//  Created by  jueying on 15/12/24.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import "MLCaptureVideoPreview.h"


@implementation MLCaptureVideoPreview

+ (Class)layerClass
{
	return [AVCaptureVideoPreviewLayer class];
}

- (instancetype)init
{
	self = [super init];
	
	if (self) {
		[self configure];
	}
	
	return self;
}

- (void)awakeFromNib
{
	[self configure];
}

- (void)configure
{
	[(AVCaptureVideoPreviewLayer *)self.layer setVideoGravity:AVLayerVideoGravityResize];
}

- (AVCaptureSession *)session
{
	AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.layer;
	return previewLayer.session;
}

- (void)setSession:(AVCaptureSession *)session
{
	AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.layer;
	previewLayer.session = session;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
