//
//  MLCameraViewController.m
//  ImagePicker
//
//  Created by  jueying on 15/12/23.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import "MLCameraViewController.h"
#import "MLCaptureVideoPreview.h"
#import <Photos/Photos.h>
#import "MLPhotoCell.h"
#import "MLPhoto.h"

@interface MLCameraViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
{
	__weak IBOutlet UICollectionView *_collectionView;
	
	void (^_completion)(NSArray <MLPhoto *> *photos);
}

@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

@property (weak, nonatomic) IBOutlet MLCaptureVideoPreview *preview;
@property (strong, nonatomic) IBOutlet AVCaptureSession *session;

@property (nonatomic, strong) AVCaptureConnection *connection;
@property (nonatomic, strong) AVCaptureDevice *device;

@end

@implementation MLCameraViewController

- (void) whenCompletion:(void (^)(NSArray <MLPhoto *> *photos))completion
{
	_completion = completion;
}

- (UINavigationController *)wrapWithNavi
{
	return [[UINavigationController alloc]initWithRootViewController:self];
}

+ (instancetype)instanceFromStoryboard
{
	return [[UIStoryboard storyboardWithName:@"MLImagePicker" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
//	[self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
	
	UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 380, 45)];
	statusBarView.backgroundColor=[UIColor blackColor];
	 [self.navigationController.navigationBar addSubview:statusBarView];
	

	self.photos = [NSMutableArray array];
	
	//初始化设备
	self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	
	//为session添加输入
	[self.session addInput:[AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil]];
	//session预设定值
	self.session.sessionPreset = AVCaptureSessionPresetPhoto;
	//为预览View 绑定session
	self.preview.session = self.session;
	//开启捕捉会话
	[self.session startRunning];
	//初始化图片捕捉输出
	self.stillImageOutput = [[AVCaptureStillImageOutput alloc]init];
	//设定图片捕捉输出
	self.stillImageOutput.outputSettings = @{AVVideoCodecKey : AVVideoCodecJPEG};
	//把图片捕捉输出添加到session
	[self.session addOutput:self.stillImageOutput];
	
}

//从看到的预览图像中截取一张图片
- (IBAction)cameraCaptureDown:(id)sender
{
	//捕捉连接
	AVCaptureConnection *connection = [self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
	AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.preview.layer;
	//矫正一次朝向
	connection.videoOrientation = previewLayer.connection.videoOrientation;
	//开始捕捉图片
	[self.stillImageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
		//如果捕捉成功了
		if (imageDataSampleBuffer) {
			NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
			//实例化相片对象
			MLPhoto *photo = [MLPhoto photoWithData:imageData];
			
			if (photo) {
				//添加到数据源
				[self.photos addObject:photo];
				[self _reloadCollectionView];
			}
		}
	}];
}

- (void)_reloadCollectionView
{
	[_collectionView reloadData];
	
	[_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.photos.count - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}

- (IBAction)cancelBtnDown:(id)sender {
	if (self.navigationController) {
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	} else {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (IBAction)okBtnDown:(id)sender {
	
	if (_completion) {
		_completion(self.photos);
	}
}

#pragma mark -- delegate 

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	MLPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MLPhotoCell class]) forIndexPath:indexPath];
	
	MLPhoto *photo = self.photos[indexPath.row];
	
	[self configureCell:cell withPhoto:photo];
	
	[cell whenDeleteBtnDown:^(MLPhotoCell *cell) {
		[self.photos removeObjectAtIndex:indexPath.row];
		[collectionView reloadData];
	}];
	
	return cell;
}

- (void)dealloc
{
	_collectionView.dataSource = nil;
	_collectionView.delegate = nil;
}

- (void)configureCell:(MLPhotoCell *)cell withPhoto:(MLPhoto *)photo
{
	cell.imageView.image = photo.image;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
