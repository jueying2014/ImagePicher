//
//  MLCameraViewController.h
//  ImagePicker
//
//  Created by  jueying on 15/12/23.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPhoto.h"

@interface MLCameraViewController : UIViewController

+ (instancetype)instanceFromStoryboard;

- (UINavigationController *)wrapWithNavi;

@property (nonatomic, strong) NSMutableArray *photos;

- (void) whenCompletion:(void (^)(NSArray <MLPhoto *> *photos))completion;

@end
