//
//  ViewController.m
//  ImagePicker
//
//  Created by  jueying on 15/12/23.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import "ViewController.h"
#import "MLCameraViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)imagePcikerShow:(id)sender {
	[self showViewController:[[MLCameraViewController instanceFromStoryboard]wrapWithNavi] sender:self];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
