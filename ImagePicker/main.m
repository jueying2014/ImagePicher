//
//  main.m
//  ImagePicker
//
//  Created by  jueying on 15/12/23.
//  Copyright © 2015年 mlt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
